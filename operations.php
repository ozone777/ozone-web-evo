<?php

class OOperations {
	
	static function hello_world() {
		
		write_log("Hello world desde OOperations");
	}
	
	static function get_payment_info($woo_subscription_id) {
		
		$user = wp_get_current_user();
		$osubscriptions = get_user_meta( $user->ID, "osubscriptions", true );
		$osubscriptions = unserialize($osubscriptions);
		return $osubscriptions[$woo_subscription_id];
	}
	
	static function get_info($user_id,$index,$meta_key,$key) {
		
		$osubscriptions = get_user_meta($user_id,$meta_key, true );
		$osubscriptions = unserialize($osubscriptions);
		$object = $osubscriptions[$index];
		if(isset($object)){
			return $object[$key];
		}else{
			return null;
		}
	}
	
    static function insert_info($args) {
		
		$user_id = $args["user_id"];
		$index= $args["index"];
		$key = $args["key"];
		$value = $args["value"];
		$meta_key = $args["meta_key"];
		
		$pay_user = get_user_by('id', $user_id);
		$meta = get_user_meta( $pay_user->ID, $meta_key, true );
		
		$array = unserialize($meta);
		if(isset($array[$index])){
			//SI ESTA CREADO EL ARRAY
			$inside_array = $array[$index];
			$inside_array[$key] = $value;
			$array[$index] = $inside_array;
		}else{
			//SI NO ESTA CREADO EL ARRAY
			$array[$index] = [];
			$array[$index] = [$key => $value];
		}	
		$serialized_array = serialize($array);
		update_user_meta( $pay_user->ID, $meta_key, $serialized_array);
		
    }
	
    static function insert_info_array($user_id,$meta_key,$index,$data) {
		
		$meta = get_user_meta( $user_id, $meta_key, true );
		$array = unserialize($meta);
				
		foreach($data as $args) {	
			
			$key = $args["key"];
			$value = $args["value"];
		
			if(isset($array[$index])){
				//SI ESTA CREADO EL ARRAY
				$inside_array = $array[$index];
				$inside_array[$key] = $value;
				$array[$index] = $inside_array;
			}else{
				//SI NO ESTA CREADO EL ARRAY
				$array[$index] = [];
				$array[$index] = [$key => $value];
			}	
			$serialized_array = serialize($array);
		}
		
		update_user_meta( $user_id, $meta_key, $serialized_array);
		
    }
}
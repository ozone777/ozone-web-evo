<?php

/*MULTI CURRENCY LOGIC*/

/*
##############################
##############################
WOOCOMMERCE CHECKOUT VIEW
##############################
##############################
*/

//SAVE IN CACHE THE OCURRENCY LANG INFO
function get_ocurrency_param(  ) 
{
    if( function_exists('WC') && isset($_GET["ocurrency"]) ){
        WC()->session->set( 'ocurrency', $_GET["ocurrency"]);
    }
};
add_action( 'woocommerce_init', 'get_ocurrency_param', 10, 0 );


//FILTER CALL TO FORMAT CURRENCY
function woocommerce_cart_product_subtotal_ocallback($object, $product, $quantity ){
	write_log("entro woocommerce_cart_product_subtotal_ocallback");
	if($product->is_taxable() ) {
		return $product_subtotal ;
	}else
	{
		$ocurrency = "USD";
		$ocurrency  = WC()->session->get( 'ocurrency' );
		$ocurrency = strtoupper($ocurrency);
		global $woocommerce_wpml;  
		$price = $woocommerce_wpml->multi_currency->prices->get_product_price_in_currency( $product->get_id(), $ocurrency );
	
    	$row_price = $price * $quantity;
   	 	$product_subtotal = wc_price( $row_price );	
		write_log("woocommerce_cart_product_subtotal_ocallback: ".$product_subtotal);
		return $product_subtotal;
	}
}
add_filter( 'woocommerce_cart_product_subtotal', 'woocommerce_cart_product_subtotal_ocallback', 10, 3 );


//SET HTML SELECT
function set_html_for_currency_selector(){
	 ?>
  		<select class="select-css" id="select_ocurrency">
     	   	<option>Selecciona una moneda</option>
     	  	<option value="USD">USD Dollar</option>
     		<option value="COP">COP Peso Colombiano</option>
   	 	</select>
		<br />
	 <?php
}
add_action( 'woocommerce_before_checkout_form', 'set_html_for_currency_selector', 20, 0 );

//SET JS SELECT LOGIC
function set_js_for_currency_selector(){
    ?>
		  <script>
	
		  	ocurrency  = "<?php echo WC()->session->get( 'ocurrency' );?>";
	
		  	jQuery( "#select_ocurrency" ).change(function() {
	  
		  	  url = origin + pathname + "?ocurrency="+jQuery(this).val();
		  	  window.location.href = url;
		  	});
	
		  	var pathname = window.location.pathname; // Returns path only (/path/example.html)
		  	var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
		  	var origin   = window.location.origin; 
	
		  	jQuery(document).ready( function (e) 
		  	{
		  		//ocurrency = getUrlParameter('ocurrency');
		  		if(ocurrency ==""){
		  			ocurrency = "USD";
		  		}
		  		jQuery("#select_ocurrency").val(ocurrency);
	
		  	});
	
		  </script>
    <?php
}
add_action( 'woocommerce_before_checkout_form', 'set_js_for_currency_selector', 21, 0 );

/*
##############################
##############################
END
##############################
##############################
*/


/*
##############################
##############################
WOOCOMMERCE MY SUBSCRIPTION VIEW
##############################
##############################
*/

#/Users/pedroconsuegra/Sites/ozonegroup/wp-content/plugins/woocommerce-subscriptions/templates/myaccount/

/*MY SUBSCRIPTION INFO*/
function woocommerce_order_formatted_line_subtotal_ocallback($subtotal, $item, $this_object ){
	
	include_once('operations.php');
	$product_id = $item["product_id"];
	$woo_subscription_id = $item["order_id"];	
	$user_id = get_current_user_id();
	$price = OOperations::get_info($user_id,$woo_subscription_id,"osubscriptions","price");
	
	if(isset($price)){
		return wc_price($price);
	}else{
		return $subtotal;
	}
}

add_filter( 'woocommerce_order_formatted_line_subtotal', 'woocommerce_order_formatted_line_subtotal_ocallback', 10, 3 );

/*SUPPORT SUBSCRIPTION REACTIVATION*/

/*MY SUBSCRIPTION SUPPORT FOR SUBSCRIPTION REACTIVATION*/
function woocommerce_can_subscription_be_updated_to__ocallback( $new_status, $can_be_updated, $this_object ){
	write_log("peter in woocommerce_can_subscription_be_updated_to__ocallback");
	$can_be_updated = true;
	return $can_be_updated;
}

add_filter( 'woocommerce_can_subscription_be_updated_to_', 'woocommerce_can_subscription_be_updated_to__ocallback', 10, 3 );
//return apply_filters( 'woocommerce_can_subscription_be_updated_to_' . $new_status, $can_be_updated, $this );



//SET CSS SELECT
function ozone_web_evo_scripts() {
    wp_enqueue_style('oselect_css', plugins_url('oselect_css.css', __FILE__));
}
add_action('wp_enqueue_scripts', 'ozone_web_evo_scripts');






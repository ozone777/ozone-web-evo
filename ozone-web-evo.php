<?php
/**
 * Plugin Name: Webevo Ozone  
 * Plugin URI: http://ozonegroup.co
 * Description: This plugin add special behavior to Ozone Web Services.
 * Version: 1.0
 * Author: Peter Consuegra
 * Author URI: http://ozonegroup.co
 * License: GPL2
 */

//INCLUDE SIMPLE MULTICURENCY LOGIC
//include_once( 'multi_currency.php' );
include( plugin_dir_path( __FILE__ ) . 'multi_currency.php');
include( plugin_dir_path( __FILE__ ) . 'operations.php');

//DEBUG LOGIC
if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

add_action('init', 'otesting');

function otesting(){
	/*
	$dir = plugin_dir_path( __FILE__ );
	$user = get_user_by('id', 12);
	OOperations::hello_world();
	
	$oarray = [];
	$woo_subscription_id = 13523;
	//$woo_subscription_id =13521;
	array_push($oarray,["key" => "subscription_id","value" => "77777"]);
	array_push($oarray,["key" => "order_currency","value" => "COP"]);
	array_push($oarray,["key" => "gateway","value" => "PayU"]);
	array_push($oarray,["key" => "client_id","value" => 777777]);
	array_push($oarray,["key" => "price","value" => "59000"]);
	array_push($oarray,["key" => "plan_code","value" => "pppp-plan-code"]);
	
	OOperations::insert_info_array(12,"osubscriptions",$woo_subscription_id, $oarray);
	*/
	write_log("desde otesting");
	$woo_subscription = new WC_Subscription(13494);
	$woo_subscription->update_status('Active');
	
}

//FAST CHECKOUT LOGIC
function fast_checkout() {
	if(isset($_POST['basic_plan'])){ 
	  WC()->cart->empty_cart();
	  WC()->cart->add_to_cart( 12057, 1 );
	}else if(isset($_POST['premium_plan'])) {
	   WC()->cart->empty_cart();
	   WC()->cart->add_to_cart( 12029, 1 );
	}else if(isset($_POST['starter_plan'])) {
	   WC()->cart->empty_cart();
	   WC()->cart->add_to_cart( 12012, 1 );
	}
}
add_action('wp_loaded', 'fast_checkout');

//REMOVE DEFAULT DASHBOARD LINK IN MY ACCOUNT
function bbloomer_remove_address_my_account( $items ) {
unset($items['dashboard']);
return $items;
}
//add_filter( 'woocommerce_account_menu_items', 'bbloomer_remove_address_my_account', 999 );


//ADD DASHBOARD LINK WITH TRANSLATIONS TO MY-ACCOUNT
function misha_one_more_link( $menu_links ){

$new = array( 'redirect-to-dashboard' => __('Dashboard','go_to_dashboard')  );

$menu_links = array_slice( $menu_links, 0, 1, true )
+ $new
+ array_slice( $menu_links, 1, NULL, true );

return $menu_links;

}
//add_filter ( 'woocommerce_account_menu_items', 'misha_one_more_link' );


//WOOCOMMERCE CHECKOUT FIELDS OPTIMIZATION
function custom_remove_woo_checkout_fields( $fields ) {

    // remove billing fields
   // unset($fields['billing']['billing_first_name']);
   // unset($fields['billing']['billing_last_name']);
    //unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
  //  unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
  //  unset($fields['billing']['billing_email']);
   
    // remove shipping fields 
    unset($fields['shipping']['shipping_first_name']);    
    unset($fields['shipping']['shipping_last_name']);  
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_address_1']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['shipping']['shipping_city']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);
    
    // remove order comment fields
    unset($fields['order']['order_comments']);
    
    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_remove_woo_checkout_fields' );
add_filter( 'woocommerce_cart_needs_shipping_address', '__return_false');

//REDIRECT AFTER PURCHASE HOOK
add_action( 'template_redirect', 'thankyou_custom_payment_redirect');
function thankyou_custom_payment_redirect(){
	
    if ( is_wc_endpoint_url( 'order-received' ) ) {
		
		if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
		  write_log("lang: ".ICL_LANGUAGE_CODE);
		  $lang = ICL_LANGUAGE_CODE;
		}
		
		write_log("Redirect to dashboard");
        wp_redirect("http://dashboard.ozonegroup.test/start_brief?lang=$lang");
        
	}
}

